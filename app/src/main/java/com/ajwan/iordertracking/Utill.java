package com.ajwan.iordertracking;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utill {

    public static final String WORKMANAGER_TAG = "Utill";
    public static final String CLOSE_SYSTEM_ALERT = "CLOSE_SYSTEM_ALERT";
    public static final String LOCATION_LAT = "LOCATION_LAT";
    public static final String LOCATION_LAG = "LOCATION_LAG";
    public static final String LOCATION_USERID = "LOCATION_USERID";
    public static final boolean TIMER_FLAG = true;
    public static final boolean TACK_MODE = true;
    public static final String LOCATION_UPDATE_MODE = "LOCATION_UPDATE_MODE";
    public static final String MANUAL_UPDATE_MODE = "MANUAL_UPDATE_MODE";
    public static final String AUTO_UPDATE_MODE = "AUTO_UPDATE_MODE";
    //public static final int TIME_LIMIT = 50;//  SEC
    //public static final int TIMER_DELAY = 50000; //  SEC
    //public static final int TIMER_DELAY = 10000 * 5; //  SEC
    //public static final int TIME_LIMIT = 60 * 5;//  MIN
    public static final int TIME_LIMIT = 2;//  MIN
    public static final int TIMER_DELAY = 1000 * 60 * 2; //  MIN
    public static final int LOC_GPS_MODE = 1;
    public static final int LOC_NETWORK_MODE = 2;
    public static final int LOC_MODE_NULL = 0;

    public static void appendLog(String text) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/iot/logs";
        try {
            File logFile = new File(fullPath);
            if (!logFile.exists()) {
                logFile.mkdirs();
            }
        } catch (Exception exp) {
            // TODO Auto-generated catch block
            exp.printStackTrace();
        }

        String fileName = "logs"+Utill.getFileName()+".txt";
        File logFile = new File(fullPath + "/"+fileName);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            String message = getTimeStamp() + "--" + text;
            buf.append(message);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.US);
        String timeStamp = sdf.format(new Date());
        return timeStamp;
    }

    public static String getFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy", Locale.US);
        String timeStamp = sdf.format(new Date());
        return timeStamp;
    }

    public static String getMLocation(double value) {
        DecimalFormat threeDForm = new DecimalFormat("#.######");
        String stringValue = Double.valueOf(threeDForm.format(value)).toString();
        return stringValue;
    }

    public static String getALocation(double value) {
        DecimalFormat threeDForm = new DecimalFormat("#.#######");
        String stringValue = Double.valueOf(threeDForm.format(value)).toString();
        return stringValue;
    }
}
