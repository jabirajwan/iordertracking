package com.ajwan.iordertracking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;



/**
 * @author Sasikumar
 * 
 */
public class MainActivity extends Activity implements ResponseListener,
		iOrderConstants {

	private TextView logoutButton;
	private WebView webView;
	private ProgressBar progressBar;

	private String userId = "", userName = "";

	// private ProgressDialog barProgressDialog;
	private TextView usernameTextView;

	private final static int REQUEST_CODE = 10101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		logoutButton = (TextView) findViewById(R.id.logoutButton);

		webView = (WebView) findViewById(R.id.webView);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);

		usernameTextView = (TextView) findViewById(R.id.usernameTextView);

		setUpController();

	}

	private void setUpController() {

		logoutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showLogoutAlertMessage();
			}

		});

		// webView.getSettings().setLoadWithOverviewMode(true);
		// webView.getSettings().setUseWideViewPort(true);

		webView.getSettings().setJavaScriptEnabled(true);
		// webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		// webView.setScrollbarFadingEnabled(false);

		userId = iOrderStatic.getPrefs(this).getString(USERID, "0");

		webView.loadUrl(ConnectionDetails.USER_DETAILS_URL + userId);

		webView.setWebViewClient(new WebViewClient(

		) {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progressBar.setVisibility(View.GONE);
			}
			
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				progressBar.setVisibility(View.GONE);
			}

		});

		usernameTextView.setText(iOrderStatic.getPrefs(this).getString(
				USERNAME, ""));

	}

	@Override
	protected void onResume() {
		super.onResume();

		checkLocationmanager();

	}

	private void checkLocationmanager() {
		final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			System.out.println("IOT Service MainActivity  Call");
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if (!Settings.canDrawOverlays(this)) {
					// If not, form up an Intent to launch the permission request
					Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));

					// Launch Intent, with the supplied request code
					startActivityForResult(intent, REQUEST_CODE);
				}else {
					sendBroadcastMsg();
					startService(new Intent(getApplicationContext(), LocationService.class));
				}
			}else {
				sendBroadcastMsg();
				startService(new Intent(getApplicationContext(), LocationService.class));
			}
		} else {
			buildAlertMessageNoGps();
		}
	}

	/*private void loginLogout(){
		try{
			Utill.appendLog("********************Application Logout********************");
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}*/

	/*private void startWorkManager(){
		Utill.appendLog("WorkManager call");
		if(!isWorkManagerRunning()) {
			Utill.appendLog("Start WorkManager");
			PeriodicWorkRequest mPeriodicWorkRequest = new PeriodicWorkRequest.Builder(MyPeriodicWork.class,
					20, TimeUnit.SECONDS)
					.addTag(Utill.WORKMANAGER_TAG)
					.build();

			WorkManager.getInstance().enqueue(mPeriodicWorkRequest);
		}
	}*/

	/*private boolean isWorkManagerRunning(){
		WorkManager instance = WorkManager.getInstance();
		ListenableFuture<List<WorkInfo>> statuses = instance.getWorkInfosByTag(Utill.WORKMANAGER_TAG);
		try {
			boolean running = false;
			List<WorkInfo> workInfoList = statuses.get();
			for (WorkInfo workInfo : workInfoList) {
				WorkInfo.State state = workInfo.getState();
				running = state == WorkInfo.State.RUNNING | state == WorkInfo.State.ENQUEUED;
			}
			return running;
		} catch (ExecutionException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}*/



	/*public static final int MILLISECONDS_PER_SECOND = 1000;

	// The update interval
	public static final int UPDATE_INTERVAL_IN_SECONDS = 2;

	// A fast interval ceiling
	public static final int FAST_CEILING_IN_SECONDS = 1;

	// Update interval in milliseconds
	public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND
			* UPDATE_INTERVAL_IN_SECONDS;

	// A fast ceiling of update intervals, used when the app is visible
	public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND
			* FAST_CEILING_IN_SECONDS;

	// A request to connect to Location Services
	private LocationRequest mLocationRequest;

	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	public Location getLocation1(Context mContext) {
		try {

			if (mLocationClient != null) {
				return null;
			}

			mLocationRequest = LocationRequest.create();

			*//*
			 * Set the update interval
			 *//*
			mLocationRequest.setInterval(iOrderStatic.getPrefs(this).getInt(
					TIMER, 5000));

			// Use high accuracy
			mLocationRequest
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

			// Set the interval ceiling to one minute
			mLocationRequest
					.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);

			mLocationClient = new LocationClient(mContext, this, this);
			mLocationClient.connect();

			// mLocationClient.requestLocationUpdates(mLocationRequest, this);
			// Location currentLocation = mLocationClient.getLastLocation();

			*//*
			 * latitude = currentLocation.getLatitude(); longitude =
			 * currentLocation.getLongitude();
			 * 
			 * if (latitude != 0 && longitude != 0) { String url =
			 * ConnectionDetails.TRACKLOCATION_URL + "UserID=" +
			 * mIntent.getStringExtra(iOrderConstants.USERID) + "&latitude=" +
			 * latitude + "&longitude=" + longitude;
			 * 
			 * // if(oldLatitude != latitude || oldLongitude != longitude) {
			 * System
			 * .out.println("sasikumar check sendrequest new = "+latitude+
			 * " === " +longitude);
			 * System.out.println("sasikumar check sendrequest old = "
			 * +oldLatitude+" === "+oldLongitude); oldLatitude = latitude;
			 * oldLongitude = longitude;
			 * System.out.println("sasikumar check sendrequest oldc= "
			 * +oldLatitude+" === "+oldLongitude); new
			 * ConnectionRequest(this).execute(url); }else{
			 * System.out.println("sasikumar check skip"); }
			 * 
			 * 
			 * }
			 *//*

			*//*
			 * locationManager = (LocationManager) mContext
			 * .getSystemService(mContext.LOCATION_SERVICE);
			 * 
			 * // getting GPS status isGPSEnabled = locationManager
			 * .isProviderEnabled(LocationManager.GPS_PROVIDER);
			 * 
			 * // getting network status isNetworkEnabled = locationManager
			 * .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			 * 
			 * System.out.println("sasikumar isGPSEnabled=" + isGPSEnabled); //
			 * System.out // .println("sasikumar isNetworkEnabled=" +
			 * isNetworkEnabled);
			 * 
			 * if (!isGPSEnabled && !isNetworkEnabled) { // no network provider
			 * is enabled
			 * 
			 * Toast.makeText(mContext, "Enable GPS",
			 * Toast.LENGTH_SHORT).show();
			 * 
			 * } else { this.canGetLocation = true; if (isNetworkEnabled) {
			 * locationManager.requestLocationUpdates(
			 * LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
			 * MIN_DISTANCE_CHANGE_FOR_UPDATES, this); Log.d("Network",
			 * "Network"); if (locationManager != null) { location =
			 * locationManager
			 * .getLastKnownLocation(LocationManager.NETWORK_PROVIDER); if
			 * (location != null) { latitude = location.getLatitude(); longitude
			 * = location.getLongitude(); } } } // if GPS Enabled get lat/long
			 * using GPS Services if (isGPSEnabled) { if (location == null) {
			 * locationManager.requestLocationUpdates(
			 * LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
			 * MIN_DISTANCE_CHANGE_FOR_UPDATES, this); Log.d("GPS Enabled",
			 * "GPS Enabled"); System.out.println("sasikumar isGPSEnabled 111");
			 * if (locationManager != null) {
			 * System.out.println("sasikumar isGPSEnabled 222"); location =
			 * locationManager
			 * .getLastKnownLocation(LocationManager.GPS_PROVIDER); if (location
			 * != null) { System.out.println("sasikumar isGPSEnabled 333");
			 * latitude = location.getLatitude(); longitude =
			 * location.getLongitude();
			 * 
			 * System.out.println("sasikumar check sendrequest new k = "+location
			 * .
			 * getLatitude()+" === "+location.getLongitude()+"==="+location.getTime
			 * ()); } } } } }
			 *//*

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
*/
	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, Enable it")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(
									@SuppressWarnings("unused") final DialogInterface dialog,
									@SuppressWarnings("unused") final int id) {
								startActivity(new Intent(
										Settings.ACTION_LOCATION_SOURCE_SETTINGS));
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,
							@SuppressWarnings("unused") final int id) {
						dialog.cancel();
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void showLogoutAlertMessage() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Do you want to logout ?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(
									@SuppressWarnings("unused") final DialogInterface dialog,
									@SuppressWarnings("unused") final int id) {
								Intent intent = new Intent(MainActivity.this,
										LoginActivity.class);
								intent.putExtra(LOGOUT_SCREEN, true);
								startActivityForResult(intent, REQUESTCODE);
								MainActivity.this.finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,
							@SuppressWarnings("unused") final int id) {
						dialog.cancel();
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void handleResponse(String response) {
		System.out.println("sasikumar response=" + response);
		// barProgressDialog.dismiss();
		// parser(response);
	}

	@Override
	public void handleError(Exception exception) {
		// barProgressDialog.dismiss();
	}

	@Override
	public void onBackPressed() {
		// super.onBackPressed();

		showLogoutAlertMessage();

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == LOGINSCREEN_RESULTCODE) {
			setResult(resultCode);
			finish();
		}else if (requestCode == REQUEST_CODE) {
			// Double-check that the user granted it, and didn't just dismiss the request
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if (Settings.canDrawOverlays(this)) {
					sendBroadcastMsg();
					startService(new Intent(getApplicationContext(), LocationService.class));
				}
				else {
					Toast.makeText(this, "Sorry. Can't draw overlays without permission...", Toast.LENGTH_SHORT).show();
					finish();
				}
			}else {
				sendBroadcastMsg();
				startService(new Intent(getApplicationContext(), LocationService.class));
			}
		}
		else {
			super.onActivityResult(requestCode, resultCode, data);
		}

	}

	private void sendBroadcastMsg(){
		try {
			Intent intent = new Intent(Utill.CLOSE_SYSTEM_ALERT);
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

}
