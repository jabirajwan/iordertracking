package com.ajwan.iordertracking;

/**
 * @author Sasikumar
 * 
 */
public interface ResponseListener {

	public void handleResponse(String response);

	public void handleError(Exception exception);

}
