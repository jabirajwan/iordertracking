package com.ajwan.iordertracking

import android.app.IntentService
import android.content.Intent
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit

class LocationUpdateService: IntentService("LOCATOIN") {
    override fun onHandleIntent(intent: Intent?) {
        when(intent?.action){
            LocationUpdateReceiver.ACTION_PROCESS_UPDATES->{
                updateLocation(intent)
            }
        }
    }

    private fun updateLocation(intent: Intent?){
        val userId = intent?.extras?.getString(Utill.LOCATION_USERID)?:"0"
        val latitude = intent?.extras?.getString(Utill.LOCATION_LAT)?:"0"
        val longitude = intent?.extras?.getString(Utill.LOCATION_LAG)?:"0"
        val updateMode = intent?.extras?.getString(Utill.LOCATION_UPDATE_MODE)?:"0"

        val url = ConnectionDetails.TRACKLOCATION_URL + "UserID=" + userId +"&latitude=" + latitude + "&longitude= "+ longitude
        getWebservice(url,updateMode)

    }

    private fun getWebservice(urlStr: String,updateMode: String) {
        Utill.appendLog("LocationUpdateService : updateMode = $updateMode")
        if (Utill.TACK_MODE) {
            Utill.appendLog("LocationUpdateService : getWebservice() : $urlStr")
        }
        val client = OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .build()
        val request = Request.Builder()
            .url(urlStr).build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Utill.appendLog("LocationUpdateService : getWebservice() : REQ FAIL " + e.message)
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    //if (Utill.TACK_MODE) {
                        Utill.appendLog("LocationUpdateService : getWebservice() LoCATION UPDATE SUCCESS")
                    //}
                } catch (ex: Exception) {
                    Utill.appendLog("LocationUpdateService : getWebservice() : PARSE FAIL " + ex.message)
                    ex.printStackTrace()
                }
                if (Utill.TACK_MODE) {
                    Utill.appendLog("**********")
                    Utill.appendLog("  ")
                }
            }
        })
    }
}