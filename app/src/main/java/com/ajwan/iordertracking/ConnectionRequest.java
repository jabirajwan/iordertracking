package com.ajwan.iordertracking;



import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Sasikumar
 * 
 */
public class ConnectionRequest extends AsyncTask<String, Void, Object> {

	private ResponseListener listener;

	public ConnectionRequest(ResponseListener listener) {
		System.out.println("ConnectionRequest : ConnectionRequest()");
		this.listener = listener;
	}

	@Override
	protected Object doInBackground(String... arg0) {
		System.out.println("ConnectionRequest : doInBackground()");
		String URL = arg0[0];

		/*String response = "";
		HttpGet httpGet = new HttpGet(URL);

		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(httpGet);
			response = EntityUtils.toString(httpResponse.getEntity());
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return e;
		}*/

		try {
			Utill.appendLog("ConnectionRequest: - 1 ");
			java.net.URL url = new URL(URL);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setReadTimeout(10000);
			urlConnection.setConnectTimeout(20000);
			urlConnection.setRequestMethod("GET");
			urlConnection.setRequestProperty("Connection", "Keep-Alive");
			urlConnection.setRequestProperty("Cache-Control", "no-cache");
			urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			// Starts the query
			Utill.appendLog("ConnectionRequest: - 2 ");
			urlConnection.connect();
			Utill.appendLog("ConnectionRequest: - 3 ");

			int statusCode = urlConnection.getResponseCode();
			if (statusCode == 200) {
				System.out.println("IOT Service HttpURLConnection SEND SUCCESS");
				String response = IOUtils.toString(urlConnection.getInputStream());
				//emitter.onSuccess(response);
				//Utill.appendLog("ConnectionRequest: SUCCESS");
				return response;
			} else {
				//emitter.onSuccess(new Object());
				Utill.appendLog("ConnectionRequest: FAILURE ");
				Utill.appendLog("FAILURE : URL "+URL);
				return new Object();
			}
		}catch (Exception ex){
			//clearQueue();
			Utill.appendLog("ConnectionRequest: Exception : "+ex.getMessage());
			Utill.appendLog("Exception : URL "+URL);
			ex.printStackTrace();
			return new Object();
			//appendLog("IOT Service Connection ERROR : "+ex.getMessage());
			//emitter.onError(new NullPointerException());
		}
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		System.out.println("ConnectionRequest : onPostExecute()");
		try {
			if (result instanceof String) {
				System.out.println("ConnectionRequest : onSuccess()");
				listener.handleResponse((String) result);
			} else {
				System.out.println("ConnectionRequest : Failure()");
				listener.handleError(new NullPointerException());
			}
		}catch (Exception ex){
			ex.printStackTrace();
		}

	}

}
