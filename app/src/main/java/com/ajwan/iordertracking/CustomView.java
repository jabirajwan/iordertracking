package com.ajwan.iordertracking;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.Context.LOCATION_SERVICE;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_DISABLED;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_ENABLED;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_WHITELISTED;

public class CustomView extends LinearLayout implements View.OnClickListener,
        LocationListener, android.location.LocationListener {

    private final String TAG = "CustomView";
    private Context mContext;
    private String userId;

    public CustomView(@NonNull Context context) {
        super(context);
        Utill.appendLog("CustomView : "+Utill.getTimeStamp()+": Attach");
        mContext = context;
        userId = iOrderStatic.getPrefs(mContext).getString(iOrderConstants.USERID, "0");
        registerBroadCastReceiver();
        getLocation();
    }

    @Override
    public void onClick(View v) {
        Log.v(TAG, "onClick()");
        //getContext().startActivity(new Intent(getContext(), MainActivity.class));
        openApp();
        dismissSystemAlert();
    }

    @Override
    protected void onDisplayHint(int hint) {
        super.onDisplayHint(hint);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // Only fire on the ACTION_DOWN event, or you'll get two events (one for _DOWN, one for _UP)
        if (event.getAction() == KeyEvent.ACTION_DOWN) {

            // Check if the HOME button is pressed
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

                Log.v(TAG, "BACK Button Pressed");
                // As we've taken action, we'll return true to prevent other apps from consuming the event as well
                //dismissSystemAlert();
                return true;
            }
        }

        // Otherwise don't intercept the event
        return super.dispatchKeyEvent(event);
    }

    private void openApp(){
        try {
            Intent i = getContext().getPackageManager().getLaunchIntentForPackage("com.ajwan.iordertracking");
            getContext().startActivity(i);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void dismissSystemAlert(){
        try {
            disableLocationUpdate();

            WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            LayoutInflater inflater = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
            View floatyView = inflater.inflate(R.layout.alert_window, this);
            windowManager.removeView(floatyView);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Utill.appendLog("CustomView : "+Utill.getTimeStamp()+": onDetachedFromWindow");
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onLocationChanged(Location location) {
        String url = ConnectionDetails.TRACKLOCATION_URL + "UserID=" + userId
                + "&latitude=" + location.getLatitude() + "&longitude="
                + location.getLongitude();

        Utill.appendLog("CUSTOMVIEW : onLocationChanged() : "+Utill.getTimeStamp());
        //sendNotification(url);
        getWebservice(url);
        /*final Data.Builder data = new Data.Builder();
        data.putString(Utill.LOCATION_LAT,""+location.getLatitude());
        data.putString(Utill.LOCATION_LAT,""+location.getLongitude());
        data.putString(Utill.LOCATION_USERID,userId);*/

        /*Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(LocationUpdateWorker.class)
                .setConstraints(constraints)
                .setInputData(data.build())
                .build();
        WorkManager.getInstance().enqueue(oneTimeWorkRequest);*/

        //OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.from(LocationUpdateWorker.class);

        /*val compressionWork = OneTimeWorkRequest.Builder(CompressWorker::class.java)
        val data = Data.Builder()
        //Add parameter in Data class. just like bundle. You can also add Boolean and Number in parameter.
        data.putString("file_path", "put_file_path_here")
        //Set Input Data
        compressionWork.setInputData(data.build())
        //enque worker
        WorkManager.getInstance().enqueue(compressionWork.build())*/

    }

    /*private void sendNotification(final String urlStr){
        Utill.appendLog("IOT Service sendNotification() : 1 "+urlStr);

        Single<Object> objectSingle = Single.create(new SingleOnSubscribe<Object>() {
            @Override
            public void subscribe(SingleEmitter<Object> emitter) throws Exception {
                Utill.appendLog("CUSTOMVIEW : IOT Service sendNotification() : 2");
                try {
                    Utill.appendLog("CUSTOMVIEW : IOT Service sendNotification() : 3");
                    URL url = new URL(urlStr);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setReadTimeout(50000);
                    urlConnection.setConnectTimeout(50000);

                    Utill.appendLog("CUSTOMVIEW : IOT Service sendNotification() : 4");

                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("Connection", "Keep-Alive");
                    urlConnection.setRequestProperty("Cache-Control", "no-cache");
                    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    // Starts the query
                    Utill.appendLog("CUSTOMVIEW : IOT Service sendNotification() : 5");
                    urlConnection.connect();
                    Utill.appendLog("CUSTOMVIEW : IOT Service sendNotification() : 6");

                    int statusCode = urlConnection.getResponseCode();
                    if (statusCode == 200) {
                        System.out.println("CUSTOMVIEW : IOT Service HttpURLConnection SEND SUCCESS");
                        String response = IOUtils.toString(urlConnection.getInputStream());
                        Utill.appendLog("CUSTOMVIEW : LOCATION SEND SUCCESS : " + urlStr);
                        emitter.onSuccess(response);
                    } else {
                        System.out.println("IOT Service HttpURLConnection SEND FAILURE");
                        Utill.appendLog("CUSTOMVIEW : HttpURLConnection SEND FAILURE");
                        emitter.onSuccess(new Object());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Utill.appendLog("CUSTOMVIEW : IOT Service Connection ERROR : " + ex.getMessage());
                    emitter.onError(new NullPointerException());
                }

            }
        });objectSingle.subscribeOn(Schedulers.io());
        objectSingle.observeOn(AndroidSchedulers.mainThread());
        objectSingle.subscribe(new SingleObserver<Object>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(Object detail) {
                if (detail instanceof String) {
                    System.out.println("CUSTOMVIEW : IOT Service LAT AND LAN : onSuccess()");
                    //setNotification("Last Update Time : "+getTimeStamp());
                } else {
                    System.out.println("CUSTOMVIEW : IOT Service LAT AND LAN : FAILURE()");
                }
            }

            @Override
            public void onError(Throwable e) {
                System.out.println("CUSTOMVIEW : IOT Service onError()");
            }
        });
    }*/
    private void getWebservice(final String urlStr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            checkNetworkRestrictMode();
        }else{
            isPowerSaveModeOn();
        }
        if(Utill.TACK_MODE) {
            Utill.appendLog("CUSTOMVIEW : getWebservice() : " + urlStr);
        }
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();
        final Request request = new Request.Builder()
                .url(urlStr).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                Utill.appendLog("CUSTOMVIEW : getWebservice() : REQ FAIL " + e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response) {
                try {
                    if(Utill.TACK_MODE) {
                        Utill.appendLog("CUSTOMVIEW : getWebservice() : SUCCESS" + response.body().string());
                    }
                }catch (Exception ex){
                    Utill.appendLog("CUSTOMVIEW : getWebservice() : PARSE FAIL " + ex.getMessage());
                    ex.printStackTrace();
                }

            }
        });
    }

    private void registerBroadCastReceiver(){
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Utill.CLOSE_SYSTEM_ALERT);
            LocalBroadcastManager.getInstance(mContext).registerReceiver(SystemAlertViewBroadCast, intentFilter);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public BroadcastReceiver  SystemAlertViewBroadCast = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Utill.appendLog("CUSTOMVIEW : BroadcastReceiver -  action : " + action);
            switch (action){
                case Utill.CLOSE_SYSTEM_ALERT:
                    Utill.appendLog("CUSTOMVIEW : DISMISSALERT");
                    dismissSystemAlert();
                    break;
            }
        }
    };

    private boolean isPowerSaveModeOn(){
        boolean status = true;
        try {
            PowerManager powerManager = (PowerManager)
                    mContext.getApplicationContext().getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                    && powerManager.isPowerSaveMode())
            {
                Utill.appendLog("CUSTOMVIEW : POWER SAVE MODE ON");
                status = false;
            }
        }catch (Exception ex){
            status = true;
            ex.printStackTrace();
        }
        return status;
    }



    //**********************************************************************************************
    private void disableLocationUpdate(){
        try {
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    // flag for GPS status
    boolean canGetLocation = false;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 200 * 10 * 3; // 2 seconds
    // Declaring a Location Manager
    protected LocationManager locationManager;

    public Location getLocation() {
        try {

            if (ActivityCompat.checkSelfPermission(
                    mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(
                    mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Utill.appendLog("CUSTOMVIEW : PERMISSION NOT AVAILABLE");
                return null;
            }
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                // Log.e(“Network-GPS”, “Disable”);
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    // Log.e(“Network”, “Network”);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                } else
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            //Log.e(“GPS Enabled”, “GPS Enabled”);
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
            }
        } catch (Exception e) {
            Utill.appendLog("CUSTOMVIEW : getLocation() : "+e.getMessage());
            e.printStackTrace();
        }
        return location;
    }


    private void checkNetworkRestrictMode(){
        try {
            ConnectivityManager connMgr = (ConnectivityManager)
                    mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            // Checks if the device is on a metered network
            if (connMgr.isActiveNetworkMetered()) {
                // Checks user’s Data Saver settings.
                switch (connMgr.getRestrictBackgroundStatus()) {
                    case RESTRICT_BACKGROUND_STATUS_ENABLED:
                        Utill.appendLog("CUSTOMVIEW : checkNetworkRestrictMode() : RESTRICT_BACKGROUND_STATUS_ENABLED");
                    break;
                    case RESTRICT_BACKGROUND_STATUS_WHITELISTED:
                        Utill.appendLog("CUSTOMVIEW : checkNetworkRestrictMode() : RESTRICT_BACKGROUND_STATUS_WHITELISTED");
                    break;

                    case RESTRICT_BACKGROUND_STATUS_DISABLED:
                        Utill.appendLog("CUSTOMVIEW : checkNetworkRestrictMode() : RESTRICT_BACKGROUND_STATUS_DISABLED");
                    break;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
