package com.ajwan.iordertracking

import android.Manifest
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat


class ManualLocationService: IntentService("LOCATOIN") {
    private val TAG = "ManualLocationService"

    private var locationManager: LocationManager? = null


    override fun onHandleIntent(intent: Intent?) {
        //LOC_MODE = Utill.LOC_MODE_NULL;
        Log.d(TAG,"ManualLocationService() Called")
        if(Utill.TACK_MODE) {
            Utill.appendLog("Manual location Service Start")
        }

        /*val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val provider = locationManager.getBestProvider(Criteria(),true)
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(Utill.TACK_MODE) {
                Utill.appendLog("ManualLocationService : Permission Denied")
            }
            return
        }
            Log.d(TAG,"provider = $provider")
            val location = locationManager.getLastKnownLocation(provider!!)

            val userId = iOrderStatic.getPrefs(applicationContext).getString(iOrderConstants.USERID, "0")
            val intent = Intent(applicationContext, LocationUpdateService::class.java)
            intent.action = LocationUpdateReceiver.ACTION_PROCESS_UPDATES
            intent.putExtra(Utill.LOCATION_USERID, userId)
            intent.putExtra(Utill.LOCATION_UPDATE_MODE, Utill.MANUAL_UPDATE_MODE)
            intent.putExtra(Utill.LOCATION_LAT, Utill.getMLocation(location!!.latitude))
            intent.putExtra(Utill.LOCATION_LAG, Utill.getMLocation(location!!.longitude))
            startService(intent)
        }*/

         locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        //val provider = locationManager.getBestProvider(Criteria(),true)
        if(locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (Utill.TACK_MODE) {
                    Utill.appendLog("ManualLocationService : Permission Denied")
                }
                return
            }
        }
        locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            1000,      10f, gpsListener);
    }

    private val gpsListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location?) {
            val userId = iOrderStatic.getPrefs(applicationContext).getString(iOrderConstants.USERID, "0")
            val intent = Intent(applicationContext, LocationUpdateService::class.java)
            intent.action = LocationUpdateReceiver.ACTION_PROCESS_UPDATES
            intent.putExtra(Utill.LOCATION_USERID, userId)
            intent.putExtra(Utill.LOCATION_UPDATE_MODE, Utill.MANUAL_UPDATE_MODE)
            intent.putExtra(Utill.LOCATION_LAT, Utill.getMLocation(location!!.latitude))
            intent.putExtra(Utill.LOCATION_LAG, Utill.getMLocation(location!!.longitude))
            startService(intent)
            locationManager!!.removeUpdates(this);
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        }

        override fun onProviderEnabled(provider: String?) {
        }

        override fun onProviderDisabled(provider: String?) {
        }
    }
}