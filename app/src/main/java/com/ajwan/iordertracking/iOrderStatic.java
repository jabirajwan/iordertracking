package com.ajwan.iordertracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

public class iOrderStatic {

	public static boolean isNetworkEnabled(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (cm.getActiveNetworkInfo() == null)
			return false;

		return cm.getActiveNetworkInfo().isConnectedOrConnecting();

	}
	
	public static SharedPreferences getPrefs(Context ctx) {
		return ctx.getSharedPreferences(ctx.getPackageName(),
				Context.MODE_PRIVATE);
	}

}
