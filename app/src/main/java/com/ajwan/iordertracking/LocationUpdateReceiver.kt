package com.ajwan.iordertracking

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import com.google.android.gms.location.LocationResult
import java.util.*

class LocationUpdateReceiver: BroadcastReceiver() {

    private val TAG = "LUBroadcastReceiver"

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, "onReceive() context:$context, intent:$intent")

        if (intent?.action == ACTION_PROCESS_UPDATES) {
            LocationResult.extractResult(intent)?.let { locationResult ->
                val locations = locationResult.locations.map { location ->
                    MyLocationEntity(
                            latitude = location.latitude,
                            longitude = location.longitude,
                            foreground = isAppInForeground(context!!),
                            date = Date(location.time)
                    )
                   // val userId = intent?.extras?.getString(Utill.LOCATION_USERID)?:"0"
                    updateLocation(location,context)
                }
                if (locations.isNotEmpty()) {
                    //LocationRepository.getInstance(context, Executors.newSingleThreadExecutor())
                    //        .addLocations(locations)
                }
            }
        }
    }

    // Note: This function's implementation is only for debugging purposes. If you are going to do
    // this in a production app, you should instead track the state of all your activities in a
    // process via android.app.Application.ActivityLifecycleCallbacks's
    // unregisterActivityLifecycleCallbacks(). For more information, check out the link:
    // https://developer.android.com/reference/android/app/Application.html#unregisterActivityLifecycleCallbacks(android.app.Application.ActivityLifecycleCallbacks
    private fun isAppInForeground(context: Context): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val appProcesses = activityManager.runningAppProcesses ?: return false

        appProcesses.forEach { appProcess ->
            if (appProcess.importance ==
                    ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                    appProcess.processName == context.packageName) {
                return true
            }
        }
        return false
    }

    private fun updateLocation(location: Location, context: Context){
        try {
            val userId = iOrderStatic.getPrefs(context).getString(iOrderConstants.USERID, "0")
            val latitude = location.latitude
            val longitude = location.longitude
            Utill.appendLog("LUBroadcastReceiver : LAT = $latitude :: LON = $longitude")
            val intent = Intent(context.applicationContext,LocationUpdateService::class.java)
            intent.action = ACTION_PROCESS_UPDATES
            intent.putExtra(Utill.LOCATION_USERID,userId)
            intent.putExtra(Utill.LOCATION_UPDATE_MODE, Utill.AUTO_UPDATE_MODE)
            intent.putExtra(Utill.LOCATION_LAT,Utill.getALocation(latitude))
            intent.putExtra(Utill.LOCATION_LAG,Utill.getALocation(longitude))
            context.applicationContext.startService(intent)

            LocationService.lastUpdateTime = Date()
        }catch (ex: Exception){
            ex.printStackTrace()
        }
    }

    companion object {
        const val ACTION_PROCESS_UPDATES =
                "com.ajwan.iordertracking"
        /*const val ACTION_PROCESS_UPDATES =
                "com.google.android.gms.location.sample.locationupdatesbackgroundkotlin.action." +
                        "PROCESS_UPDATES"*/

        //com.google.android.gms.location.sample.locationupdatesbackgroundkotlin.LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES
    }
}