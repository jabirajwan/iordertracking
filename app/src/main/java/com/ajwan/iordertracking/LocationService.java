package com.ajwan.iordertracking;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.location.Location;
import android.os.Environment;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class LocationService extends Service  {

	private static final String TAG = "LocationService";
	private Timer timer;
//	private int LOC_MODE = 0;


	/**
	 * The desired interval for location updates. Inexact. Updates may be more or less frequent.
	 */
	//private static final long UPDATE_INTERVAL_IN_MILLISECONDS = TimeUnit.SECONDS.toMillis(60);
	private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000 * 1;

	/**
	private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
	/**
	private long FASTEST_INTERVAL = 2000; /* 2 sec */
	/**
	 * The fastest rate for active location updates. Exact. Updates will never be more frequent
	 * than this value.
	 */
	//private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = TimeUnit.SECONDS.toMillis(30);
	private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000 * 1;


	/**
	 * Provides access to the Fused Location Provider API.
	 */
	private FusedLocationProviderClient mFusedLocationClient;

	/**
	 * Provides access to the Location Settings API.
	 */
	private SettingsClient mSettingsClient;

	/**
	 * Stores parameters for requests to the FusedLocationProviderApi.
	 */
	private LocationRequest mLocationRequest;

	/**
	 * Stores the types of location services the client is interested in using. Used for checking
	 * settings to determine if the device has optimal location settings.
	 */
	private LocationSettingsRequest mLocationSettingsRequest;

	/**
	 * Callback for Location events.
	 */
	private LocationCallback mLocationCallback;

	/**
	 * Represents a geographical location.
	 */
	private Location mCurrentLocation;

	private String userId;

	public static Date lastUpdateTime = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {

		userId = iOrderStatic.getPrefs(this).getString(iOrderConstants.USERID, "0");

		System.out.println("IOT Service sasikumar service oncreate=" + userId);

		appendLog("onCreate service");
		Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();

		//disableLocationUpdate();
		if (Utill.TIMER_FLAG) {
			//stopTimer();
			startTimer();
		}
		lastUpdateTime = null;
		//LOC_MODE = Utill.LOC_MODE_NULL;

		mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
		mSettingsClient = LocationServices.getSettingsClient(this);

		createLocationCallback();
		createLocationRequest();
		buildLocationSettingsRequest();

		startLocationUpdates();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		appendLog("onStartCommand() : onStartCommand()");
		/*disableLocationUpdate();
		if (Utill.TIMER_FLAG) {
			stopTimer();
			startTimer();
		}
		lastUpdateTime = null;
		LOC_MODE = Utill.LOC_MODE_NULL;
		getLocation();*/
		return START_STICKY;
	}

	/**
	 * Creates a callback for receiving location events.
	 */
	private void createLocationCallback() {
		mLocationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {
				super.onLocationResult(locationResult);
				mCurrentLocation = locationResult.getLastLocation();
				lastUpdateTime = new Date();
				if(Utill.TACK_MODE) {
					appendLog("AutoLocationUpdate()  Location Update");
				}
				final double lat =  mCurrentLocation.getLatitude();
				final double lon = mCurrentLocation.getLongitude();
				/*String url = ConnectionDetails.TRACKLOCATION_URL + "UserID=" + userId
						+ "&latitude=" + Utill.getALocation(lat) + "&longitude="
						+ Utill.getALocation(lon);
				getWebservice(url);*/

				Intent intent = new Intent(getApplicationContext(),LocationUpdateService.class);
				intent.setAction(LocationUpdateReceiver.ACTION_PROCESS_UPDATES);
				intent.putExtra(Utill.LOCATION_USERID,userId);
				intent.putExtra(Utill.LOCATION_UPDATE_MODE,Utill.AUTO_UPDATE_MODE);
				intent.putExtra(Utill.LOCATION_LAT,Utill.getMLocation(lat));
				intent.putExtra(Utill.LOCATION_LAG,Utill.getMLocation(lon));
				startService(intent);
			}
		};
	}

	private void createLocationRequest() {
		mLocationRequest = new LocationRequest();

		// Sets the desired interval for active location updates. This interval is
		// inexact. You may not receive updates at all if no location sources are available, or
		// you may receive them slower than requested. You may also receive updates faster than
		// requested if other applications are requesting location at a faster interval.
		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

		// Sets the fastest rate for active location updates. This interval is exact, and your
		// application will never receive updates faster than this value.
		mLocationRequest.setFastestInterval( FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
		// Sets the maximum time when batched location updates are delivered. Updates may be
		// delivered sooner than this interval.
		mLocationRequest.setMaxWaitTime(TimeUnit.MINUTES.toMillis(2));

		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		//mLocationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
	}

	/**
	 * Uses a {@link LocationSettingsRequest.Builder} to build
	 * a {@link LocationSettingsRequest} that is used for checking
	 * if a device has the needed location settings.
	 */
	private void buildLocationSettingsRequest() {
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
		builder.addLocationRequest(mLocationRequest);
		mLocationSettingsRequest = builder.build();
	}

	/**
	 * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
	 * runtime permission has been granted.
	 */
	private void startLocationUpdates() {
		// Begin by checking if the device has the necessary location settings.
		mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
		.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
			@Override
			public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
				Log.i(TAG, "All location settings are satisfied.");

				//noinspection MissingPermission
				//mFusedLocationClient.requestLocationUpdates(mLocationRequest,
				//		mLocationCallback, Looper.myLooper());

				mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationUpdatePendingIntent());
			}
		})
		.addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				int statusCode = ((ApiException) e).getStatusCode();
				switch (statusCode) {
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						Log.d(TAG, "Location settings are not satisfied. Attempting to upgrade location settings ");
						appendLog("Location settings are not satisfied. Attempting to upgrade location settings");
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						String errorMessage = "Location settings are inadequate, and cannot be " +
								"fixed here. Fix in Settings.";
						Log.e(TAG, errorMessage);
						appendLog("Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
				}
			}
		});
	}

	OnLocationUpdatedListener locationUpdatedListener = new OnLocationUpdatedListener() {
		@Override
		public void onLocationUpdated(Location location) {
			final double lat = location.getLatitude();
			final double lon = location.getLongitude();
			if(Utill.TACK_MODE) {
				appendLog("smartlocation:library  Location Update");
			}
			/*final String url = ConnectionDetails.TRACKLOCATION_URL + "UserID=" + userId
					+ "&latitude=" + Utill.getMLocation(lat) + "&longitude="
					+ Utill.getMLocation(lon);
			getWebservice(url);*/
			Intent intent = new Intent(getApplicationContext(),LocationUpdateService.class);
			intent.setAction(LocationUpdateReceiver.ACTION_PROCESS_UPDATES);
			intent.putExtra(Utill.LOCATION_USERID,userId);
			intent.putExtra(Utill.LOCATION_UPDATE_MODE,Utill.MANUAL_UPDATE_MODE);
			intent.putExtra(Utill.LOCATION_LAT,Utill.getMLocation(lat));
			intent.putExtra(Utill.LOCATION_LAG,Utill.getMLocation(lon));
			startService(intent);
		}
	};



	private void startTimer() {
		appendLog("startTimer()");
		timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				appendLog("Timer Running");
				if (isNeedTimerBaseUpdate()) {
					manualLocationUpdate();
				}
			}
		}, Utill.TIMER_DELAY, Utill.TIMER_DELAY);
	}

	public void appendLog(String text) {
		String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
				"/iot/logs";
		try {
			File logFile = new File(fullPath);
			if (!logFile.exists()) {
				logFile.mkdirs();
			}
		} catch (Exception exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
		}

		String fileName = "logs" + Utill.getFileName() + ".txt";
		File logFile = new File(fullPath + "/" + fileName);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			//BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
			String message = getTimeStamp() + "--" + text;
			buf.append(message);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		appendLog("Service Destroyed");
		Toast.makeText(getApplicationContext(), "Service Destroyed", Toast.LENGTH_SHORT).show();
		if (Utill.TIMER_FLAG) {
			stopTimer();
		}
		disableLocationUpdate();
		checkSystemLogout();
	}

	private void disableLocationUpdate() {
		try {
			/*if (mLocationClient != null && mLocationClient.isConnected()) {
				System.out.println("IOT Service sasikumar service ondestroy");
				//mLocationClient.removeLocationUpdates(this);
			}
			if (locationManager != null) {
				appendLog("IOT Service - locationManager - Remove");
				//locationManager.removeUpdates(this);
			}*/
			// It is a good practice to remove location requests when the activity is in a paused or
			// stopped state. Doing so helps battery performance and is especially
			// recommended in applications that request frequent location updates.
			stopLocationUpdate();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void stopLocationUpdate(){
		appendLog("stopLocationUpdate()");
		try {
			/*mFusedLocationClient.removeLocationUpdates(mLocationCallback)
					.addOnCompleteListener(new OnCompleteListener<Void>() {
						@Override
						public void onComplete(@NonNull Task<Void> task) {
							appendLog("stopLocationUpdate() : onComplete");
						}
					});*/
			mFusedLocationClient.removeLocationUpdates(locationUpdatePendingIntent());
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	private void stopTimer() {
		try {
			appendLog("stopTimer()");
			timer.cancel();
			timer.purge();
		} catch (Exception ex) {
			appendLog("stopTimer() - Exception");
			ex.printStackTrace();
		}

	}

	public String getTimeStamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
		String timeStamp = sdf.format(new Date());
		return timeStamp;
	}

	@Override
	public boolean stopService(Intent name) {
		// TODO Auto-generated method stub
		System.out.println("IOT Service  service stop");
		appendLog("Method-stopService Service Stopped");

		/*timer.cancel();
		task.cancel();*/


		return super.stopService(name);

	}

	/*// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2; // 10 meters

	// The minimum time between updates in milliseconds
	//private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
	private static final long MIN_TIME_BW_UPDATES = 200 * 10 * 10; // 2 seconds*/
	private void getWebservice(final String urlStr) {
		if (Utill.TACK_MODE) {
			Utill.appendLog("IOT SERVICE : getWebservice() : " + urlStr);
		}
		OkHttpClient client = new OkHttpClient.Builder()
				.connectTimeout(10, TimeUnit.SECONDS)
				.readTimeout(10, TimeUnit.SECONDS)
				.writeTimeout(10, TimeUnit.SECONDS)
				.retryOnConnectionFailure(false)
				.build();
		final Request request = new Request.Builder()
				.url(urlStr).build();
		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				e.printStackTrace();
				Utill.appendLog("IOT SERVICE : getWebservice() : REQ FAIL " + e.getMessage());
			}

			@Override
			public void onResponse(Call call, final Response response) {
				try {
					lastUpdateTime = new Date();
					if (Utill.TACK_MODE) {
						Utill.appendLog("IOT SERVICE : getWebservice() LoCATION UPDATE SUCCESS");
					}
				} catch (Exception ex) {
					Utill.appendLog("IOT SERVICE : getWebservice() : PARSE FAIL " + ex.getMessage());
					ex.printStackTrace();
				}
				if(Utill.TACK_MODE) {
					Utill.appendLog("**********");
					Utill.appendLog("  ");
				}
			}
		});
	}

	private void checkSystemLogout() {
		appendLog("checkSystemLogout()");
		String userId = iOrderStatic.getPrefs(this).getString(iOrderConstants.USERID, "0");
		if (!userId.equals("0")) {
			appendLog("checkSystemLogout() : Restart service");
			//startService(new Intent(getApplicationContext(),AlertViewServise.class));
			addOverlayView();
		}
	}

	private void addOverlayView() {

		final WindowManager.LayoutParams params;
		int layoutParamsType;

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			layoutParamsType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
		} else {
			layoutParamsType = WindowManager.LayoutParams.TYPE_PHONE;
		}

		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		int width = displayMetrics.widthPixels;

		//width-(width/4)
		params = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				layoutParamsType,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
				PixelFormat.TRANSLUCENT);

		params.gravity = Gravity.CENTER | Gravity.END;
		params.x = 0;
		params.y = 0;

		LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
		//WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

		if (inflater != null) {
			appendLog("addOverlayView() : Open System Alert View");
			CustomView customView = new CustomView(this);
			View floatyView = inflater.inflate(R.layout.alert_window, customView);
			//Button btnApp = (Button)floatyView.findViewById(R.id.btnApp);
			//btnApp.setOnClickListener(customView);
			ImageView imgIOT = (ImageView) floatyView.findViewById(R.id.imgIOT);
			imgIOT.setOnClickListener(customView);
			windowManager.addView(floatyView, params);
		} else {
			Log.e("SAW-example", "Layout Inflater Service is null; can't inflate and display R.layout.floating_view");
		}
	}

	private boolean isNeedTimerBaseUpdate() {
		boolean status = false;
		if (lastUpdateTime == null) {
			return true;
		}
		try {
			final Date currentDate = new Date();
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US);
			//System.out.println("IOT SERVICE : lastUpdateTime " + sdf.format(lastUpdateTime));
			//Calendar cal = Calendar.getInstance();
			//cal.setTime(currentDate);
			//cal.add(Calendar.MINUTE, Utill.TIME_LIMIT);
			//Date calDate = cal.getTime();
			//System.out.println("IOT SERVICE : CurrentTime " + sdf.format(currentDate));
			long diff = currentDate.getTime() - lastUpdateTime.getTime();
			//long diffSec = diff / 1000;
			long diffMinutes = diff / (60 * 1000) % 60;
			if (diffMinutes > Utill.TIME_LIMIT) {
				status = true;
			}
		} catch (Exception e) {
			status = true;
			e.printStackTrace();
		}
		return status;
	}

	private void manualLocationUpdate(){
		try {
			if(Utill.TACK_MODE) {
				appendLog(" ");
				appendLog("manualLocationUpdate()  Location Update");
			}
			SmartLocation.with(this).location().start(locationUpdatedListener);
			//mFusedLocationClient.getLastLocation()
			//final Intent intent = new Intent(getApplicationContext(),ManualLocationService.class);
			//startService(intent);
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	/*private void manualLocationUpdate() {
		try {
			if (locationManager == null) {
				appendLog("manualLocationUpdate() : locationManager is Null");
				LOC_MODE = Utill.LOC_MODE_NULL;
				//getLocation();
				return;
			}
			boolean isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (!isGPSEnabled) {
				appendLog("manualLocationUpdate() : GPS DISABLED");
				switch (LOC_MODE) {
					case Utill.LOC_MODE_NULL:
					case Utill.LOC_GPS_MODE:
						//getLocation();
						break;
					case Utill.LOC_NETWORK_MODE:
						break;
				}
				return;
			}
			//Location loc = getLocationManager();
			if (location == null) {
				appendLog("manualLocationUpdate() : LOCATION IS NULL");
				//getLocation();
			}
			Location loc = null;
			//Location loc = getLocationManager();
			//final double lat = location.getLatitude();
			//final double lon = location.getLongitude();
			final double lat = loc.getLatitude();
			final double lon = loc.getLongitude();
			appendLog("manualLocationUpdate() : Manual  Location Update");
			if(Utill.TACKMODE) {
				appendLog("manualLocationUpdate() : Manual  Location Update Lat = "+lat+" : lon = "+lon);
			}
			String url = ConnectionDetails.TRACKLOCATION_URL + "UserID=" + userId
					+ "&latitude=" + Utill.getLocation(lat) + "&longitude="
					+ Utill.getLocation(lon);
			getWebservice(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/*private Location getLocationManager() {
		Location loc = null;
		switch (LOC_MODE) {
			case Utill.LOC_MODE_NULL:
				appendLog("getLocationManager() : LOC_MODE_NULL");
				loc = null;
				break;
			case Utill.LOC_GPS_MODE:
				appendLog("getLocationManager() : LOC_GPS_MODE");
				final LocationManager locManager1 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				loc = locManager1
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				break;
			case Utill.LOC_NETWORK_MODE:
				appendLog("getLocationManager() : LOC_NETWORK_MODE");
				final LocationManager locManager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				loc = locManager2
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				break;
		}
		return loc;
	}*/

	private PendingIntent locationUpdatePendingIntent()  {
		final Intent intent = new Intent(getApplicationContext(), LocationUpdateReceiver.class);
		intent.setAction(LocationUpdateReceiver.ACTION_PROCESS_UPDATES);
		//intent.putExtra(Utill.LOCATION_USERID,userId);
		return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
}
