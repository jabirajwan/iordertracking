package com.ajwan.iordertracking;

/**
 * @author Sasikumar
 * 
 */
public interface iOrderConstants {

	String USERID = "USERID";
	String TIMER = "TIMER";
	String MOBIE_NUMBER="mobile_number";
	String PASSWORD="password";
	String LOGOUT_SCREEN = "LOGOUT_SCREEN";

	String USERNAME = "USERNAME";

	int REQUESTCODE = 100;
	int MAINSCREEN_RESULTCODE = 101;
	int LOGINSCREEN_RESULTCODE = 102;

}
