package com.ajwan.iordertracking;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import androidx.annotation.NonNull;

import android.util.Xml;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import org.xml.sax.SAXException;

/**
 * @author Sasikumar
 * 
 */
public class LoginActivity extends Activity implements ResponseListener,
		iOrderConstants {

	private EditText usernameEditText, passwordEditText;
	private Button loginButton;

	private String userId = "", userName = "";
	private int timer = 0;
	private String logoutUserId = "";
	String mobileNumber,password;

	private ProgressDialog barProgressDialog;

	boolean isLogoutScreen = false;
	private TextView logoutMsgTextView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		logoutMsgTextView = (TextView) findViewById(R.id.logoutMessageText);

		usernameEditText = (EditText) findViewById(R.id.userNameText);
		passwordEditText = (EditText) findViewById(R.id.passwordText);

		loginButton = (Button) findViewById(R.id.loginButton);

		setUpController();

		if(isOverMarshmallow()){
		    //setBatteryIgnoreApp();
			requestForDeviceIdPermissions();
        }
	}

	private void setUpController() {

		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isLogoutScreen) {
					sendLogoutRequest();
				} else {
					sendLoginRequest();
				}
			}

		});

		isLogoutScreen = getIntent().getBooleanExtra(LOGOUT_SCREEN, false);

		if (isLogoutScreen) {
			logoutMsgTextView.setText(getString(R.string.login_logout_msg));
			mobileNumber = iOrderStatic.getPrefs(this).getString(MOBIE_NUMBER, "");
			password = iOrderStatic.getPrefs(this).getString(PASSWORD, "");
			usernameEditText.setText(mobileNumber);
			passwordEditText.setText(password);
			loginButton.setText("Logout");
		} else {
			String userId = iOrderStatic.getPrefs(this).getString(USERID, "0");
			if (!userId.equals("0")) {
			    finish();
				startActivity(new Intent(this, MainActivity.class));
			}
		}

	}

	private void sendLoginRequest() {

		// startService(new Intent(this, Sample.class));

		mobileNumber = usernameEditText.getText().toString().trim();
		password = passwordEditText.getText().toString();

		if (mobileNumber.length() == 0 || password.length() == 0) {
			Toast.makeText(this, "Fields empty", Toast.LENGTH_SHORT).show();
			return;
		}

		String url = ConnectionDetails.LOGIN_URL + "UserName=" + mobileNumber
				+ "&password=" + password;

		barProgressDialog = new ProgressDialog(this);
		barProgressDialog.setMessage("Authenticating...");
		barProgressDialog.show();

		//.appendLog("handleResponse() : Login Request "+url);

		new ConnectionRequest(this).execute(url);


	}

	private void sendLogoutRequest() {

		String username = usernameEditText.getText().toString().trim();
		String password = passwordEditText.getText().toString();

		if (username.length() == 0 || password.length() == 0) {
			Toast.makeText(this, "Fields empty", Toast.LENGTH_SHORT).show();
			return;
		}

		String url = ConnectionDetails.LOGOUT_URL + "UserName=" + username
				+ "&password=" + password;

		barProgressDialog = new ProgressDialog(this);
		barProgressDialog.setMessage("Logging Out...");
		barProgressDialog.show();

		new ConnectionRequest(this).execute(url);

		//setStopService();

	}

	private void setStopService(){
		try {
			stopService(new Intent(this, LocationService.class));
			Utill.appendLog("********************Application Logout********************");
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void handleResponse(String response) {
		System.out.println("sasikumar response=" + response);
		//Utill.appendLog("handleResponse() : "+response);
		barProgressDialog.dismiss();
		if (isLogoutScreen) {
			parserLogoutResponse(response);
		} else {
			//Utill.appendLog("handleResponse() : Login");
			parserLoginResponse(response);
		}
	}

	@Override
	public void handleError(Exception exception) {
		barProgressDialog.dismiss();
		Toast.makeText(this, "Problem OCCUR", Toast.LENGTH_SHORT).show();
	}

	public void parserLogoutResponse(String response) {

		RootElement root = new RootElement(ConnectionDetails.URI, "Login");

		root.getChild(ConnectionDetails.URI, "UserID")
				.setEndTextElementListener(new EndTextElementListener() {
					public void end(String body) {
						logoutUserId = body;
					}
				});

		try {
			Xml.parse(response, root.getContentHandler());
		} catch (SAXException e) {
			Toast.makeText(this, "Problem in parsing the response.",
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();
			return;
		}

		if (logoutUserId.equals("0")) {
			Toast.makeText(this, "Problem in Logging out...",
					Toast.LENGTH_SHORT).show();
		} else {

			Toast.makeText(this, "Logged out Successfully", Toast.LENGTH_SHORT)
					.show();

			//stopService(new Intent(this, CurrentLocationService.class));
            setStopService();
			SharedPreferences.Editor editor=iOrderStatic.getPrefs(this).edit();

			editor.putString(USERID, "0");
			editor.remove(USERNAME);
			editor.remove(MOBIE_NUMBER);
			editor.remove(PASSWORD);
			editor.putInt(TIMER, 0).apply();

			setResult(LOGINSCREEN_RESULTCODE);
			finish();

		}

	}

	@Override
	public void onBackPressed() {

		stopService(new Intent(this, LocationService.class));

		super.onBackPressed();
	}

	public void parserLoginResponse(String response) {

		RootElement root = new RootElement(ConnectionDetails.URI, "Login");

		root.getChild(ConnectionDetails.URI, "UserID")
				.setEndTextElementListener(new EndTextElementListener() {
					public void end(String body) {
						userId = body;
					}
				});

		root.getChild(ConnectionDetails.URI, "Timer")
				.setEndTextElementListener(new EndTextElementListener() {
					public void end(String body) {
						timer = Integer.parseInt(body);
					}
				});

		root.getChild(ConnectionDetails.URI, "UserName")
				.setEndTextElementListener(new EndTextElementListener() {
					public void end(String body) {
						userName = body;
					}
				});

		try {
			Xml.parse(response, root.getContentHandler());
		} catch (SAXException e) {
			Toast.makeText(this, "Problem in parsing the response.",
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();
			return;
		}

		System.out.println("response timer=" + timer);

		if (userId.equals("0")) {
			Toast.makeText(this, "Invalid Username or Password",
					Toast.LENGTH_SHORT).show();
		} else {

			usernameEditText.setText("");
			passwordEditText.setText("");
			SharedPreferences.Editor editor=iOrderStatic.getPrefs(this).edit();

			editor.putString(USERNAME, userName);
			editor.putString(USERID, userId);
			editor.putString(MOBIE_NUMBER, mobileNumber);
			editor.putString(PASSWORD,password);
			editor.putInt(TIMER, timer).apply();

			Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show();

			Intent intent = new Intent(this, MainActivity.class);
			startActivityForResult(intent, REQUESTCODE);
			finish();
		}
	}

	public  boolean isOverMarshmallow() {
		return Build.VERSION.SDK_INT >= 23;
	}

	private void requestForDeviceIdPermissions() {

			ActivityCompat.requestPermissions(this,new String[]{
					Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.ACCESS_COARSE_LOCATION,
					Manifest.permission.ACCESS_FINE_LOCATION,
					Manifest.permission.INTERNET,
					Manifest.permission.ACCESS_NETWORK_STATE,
					Manifest.permission.ACCESS_BACKGROUND_LOCATION
			},  1000);
	}

	private void ignorbatteryOptimaization(){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			Intent intent = new Intent();
			String packageName = getPackageName();
			PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
			if (!pm.isIgnoringBatteryOptimizations(packageName)) {
				intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
				intent.setData(Uri.parse("package:" + packageName));
				startActivity(intent);
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (requestCode == 1000) {
			if (grantResults.length > 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
				finish();
			}
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	/*public void setBatteryIgnoreApp(){

		if (checkSelfPermission(enableBatteryOptimization())
				!= PackageManager.PERMISSION_GRANTED) {

			Intent batteryIntent = new Intent();
			String packageName = getPackageName();
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			//http://developer.android.com/intl/ko/training/monitoring-device-state/doze-standby.html#support_for_other_use_cases
			if (pm.isIgnoringBatteryOptimizations(packageName))
				batteryIntent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
			else {
				batteryIntent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
				batteryIntent.setData(Uri.parse("package:" + packageName));
			}

			startActivity(batteryIntent);
		}
	}*/

	/*private String enableBatteryOptimization(){
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		String packageName = getPackageName();
		if (pm.isIgnoringBatteryOptimizations(packageName))
			return Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS;
		else {
			return Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;
		}
	}*/
	/*private void checkDrawOverlayPermission() {

		// Checks if app already has permission to draw overlays
		if (!Settings.canDrawOverlays(this)) {

			// If not, form up an Intent to launch the permission request
			Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));

			// Launch Intent, with the supplied request code
			startActivityForResult(intent, REQUEST_CODE);
		}
	}*/
}
